# POC Flutter Clean architecture

## Structure

La Clean Architecture avec Flutter se compose généralement de trois couches principales:

- La couche de présentation (Presentation) - Cette couche est responsable de l'interface utilisateur et de l'interaction avec l'utilisateur. Les widgets sont utilisés pour afficher les données et réagir aux interactions de l'utilisateur.
- La couche de logique métier (Domain) - Cette couche contient la logique métier de l'application, qui est déconnectée de l'interface utilisateur. Les blocs ou les stores sont utilisés pour gérer l'état de l'application et fournir des données à l'interface utilisateur.
- La couche d'infrastructure (Data) - Cette couche est responsable de la communication avec les sources de données externes telles que les bases de données, les API et les services web. Les repositories ou les providers sont utilisés pour encapsuler l'accès aux sources de données.

## Features

Les features sont dans le répertoire lib/features

- Home: Page d'accueil pour se connecter en tant que user ou admin ;
- Posts: Affichage des posts récupérés de l'API JsonPlaceHolder.
