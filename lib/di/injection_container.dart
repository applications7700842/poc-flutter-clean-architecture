import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import '../features/home/presentation/cubit/sign_cubit.dart';
import '../features/posts/data/datasources/post_remote_data_source.dart';
import '../features/posts/data/repositories/posts_repository_impl.dart';
import '../features/posts/domain/repositories/posts_repository.dart';
import '../features/posts/domain/usecases/get_posts_usecase.dart';
import '../features/posts/presentation/cubit/posts_cubit.dart';
import 'di_ext.dart';

part '../features/home/common/sign_injection.dart';
part '../features/posts/common/posts_injection.dart';
//import 'di_ext.dart';

final GetIt sl = GetIt.instance;
final Dio dio = Dio();

Future<void> init() async {
  _featureSign();
  _featurePosts();
}
