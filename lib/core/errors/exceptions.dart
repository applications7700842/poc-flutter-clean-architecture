class ServerException implements Exception {}

class OfflineException implements Exception {}

class TimeoutException implements Exception {}

class CacheException implements Exception {
  const CacheException();
}

class NotFoundException extends ServerException {
  NotFoundException();
}

class UnauthorisedException extends ServerException {
  UnauthorisedException();
}

class BadRequestException extends ServerException {
  BadRequestException();
}
