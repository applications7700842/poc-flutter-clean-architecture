import 'package:freezed_annotation/freezed_annotation.dart';

part 'failures.freezed.dart';

@freezed
class Failure with _$Failure {
  const factory Failure.server({String? message}) = _Server;

  const factory Failure.cache({String? message}) = _Cache;

  const factory Failure.notFound({String? message}) = _NotFound;

  const factory Failure.offline({String? message}) = _Offline;

  const factory Failure.timeout({String? message}) = _Timeout;

  const factory Failure.unauthorized({String? message}) = _Unauthorized;

  const factory Failure.badRequest({String? message}) = _BadRequest;
}
