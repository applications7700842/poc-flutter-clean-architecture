import 'dart:async';

import 'package:dio/dio.dart';

import '../../constants/api_constants.dart';
import '../../errors/exceptions.dart';

abstract class RemoteDataSource {
  RemoteDataSource({
    required this.dio,
    String? baseUrl,
  }) {
    _baseUrl = baseUrl ?? ApiConstants.baseUrl;

    _initDioClient();
  }
  late String _baseUrl;

  final Dio dio;

  int get millisecondsTimeoutWs => const Duration(seconds: 10).inMilliseconds;

  void _initDioClient() {
    dio.interceptors.clear();

    dio.interceptors.add(
      InterceptorsWrapper(
        onRequest: (options, handler) {
          options
            ..validateStatus = (status) {
              return status! < 400;
            }
            ..connectTimeout = millisecondsTimeoutWs
            ..receiveTimeout = millisecondsTimeoutWs;

          handler.next(options);
        },
        onResponse: (response, handler) {
          handler.next(response);
        },
        onError: (error, handler) async {
          handler.next(error);
        },
      ),
    );
    dio.interceptors.add(LogInterceptor(responseBody: true));
  }

  Future<T> performGetRequestApi<T>({
    required String apiEndpoint,
  }) async {
    Future<Response<T>> request() async {
      final respnse = dio.get<T>(_path(apiEndpoint));
      return respnse;
    }

    return _performRequestApi(request);
  }

  Future<T> _performRequestApi<T>(
    Future<Response<T>> Function() request,
  ) async {
    try {
      final Response<T> response = await request();
      return response.data!;
    } on DioError catch (e) {
      if (e.type == DioErrorType.connectTimeout ||
          e.type == DioErrorType.receiveTimeout ||
          e.type == DioErrorType.sendTimeout) {
        throw TimeoutException();
      }
      if (e.response?.statusCode == 401) {
        throw UnauthorisedException();
      } else if (e.response?.statusCode == 404) {
        throw NotFoundException();
      } else if (e.response?.statusCode == 400) {
        throw BadRequestException();
      } else if (e.response?.statusCode == 504) {
        throw TimeoutException();
      }
      throw ServerException();
    }
  }

  String _path(String endpoint) {
    return '$_baseUrl$endpoint';
  }
}
