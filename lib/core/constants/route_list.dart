class RouteList {
  RouteList._();

  static const String homePage = '/home';
  static const String postsRoute = '/posts';
  static const String menuPage = '/menu';
  static const String internationalization = '/internationalization';
}
