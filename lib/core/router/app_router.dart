import 'package:flutter/material.dart';

import '../../features/home/presentation/home_page.dart';
import '../../features/internationalization/presentation/internationalizationPage.dart';
import '../../features/menu/presentation/menu_page.dart';
import '../../features/posts/presentation/posts_page.dart';
import '../constants/route_list.dart';

class AppRouter {
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey();

  String get initialRoute => RouteList.homePage;

  List<Route> onGenerateInitialRoutes(String initialRoute) {
    return [getPageRoute(initialRoute, null)];
  }

  Route onGenerateRoute(RouteSettings routeSettings) {
    final String routeName = routeSettings.name ?? '';
    return getPageRoute(routeName, routeSettings.arguments);
  }

  Route getPageRoute(String routeName, Object? args) {
    switch (routeName) {
      case RouteList.homePage:
        return MaterialPageRoute(builder: (_) => const HomePage());
      case RouteList.menuPage:
        return MaterialPageRoute(builder: (_) => const MenuPage());
      case RouteList.postsRoute:
        return MaterialPageRoute(builder: (_) => const PostsPage());
      case RouteList.internationalization:
        return MaterialPageRoute(
          builder: (context) => const InternationalizationPage(),
        );
      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(
            body: Center(
              child: Text('No route defined for $routeName'),
            ),
          ),
        );
    }
  }
}
