import 'package:flutter/material.dart';

abstract class ColorSchemeApp {
  static ColorScheme demoLight() => const ColorScheme.light();

  static ColorScheme demoDark() => const ColorScheme.dark();
}
