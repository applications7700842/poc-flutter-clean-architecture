import 'package:flutter/material.dart';
import 'package:gap/gap.dart';

enum Spacing {
  none(0),
  verySmall(2),
  small(4),
  semiSmall(8),
  semiRegular(12),
  regular(14),
  semiLarge(20),
  mediumLarge(24),
  large(30),
  veryLarge(45);

  const Spacing(this.value);
  final double value;
}

class AppGap extends StatelessWidget {
  const AppGap(this.size, {super.key});

  const AppGap.small({super.key}) : size = Spacing.small;
  final Spacing size;

  @override
  Widget build(BuildContext context) => Gap(size.value);
}
