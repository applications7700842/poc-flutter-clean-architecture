import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'core/router/app_router.dart';
import 'core/theme/colors_scheme_app.dart';
import 'core/theme/typography.dart';
import 'di/injection_container.dart' as di;
import 'features/home/presentation/cubit/auth_cubit.dart';
import 'features/home/presentation/cubit/sign_cubit.dart';
import 'features/posts/presentation/cubit/posts_cubit.dart';

class DemoStart extends StatefulWidget {
  const DemoStart({super.key});

  static void startApp() async {
    WidgetsFlutterBinding.ensureInitialized();

    await di.init();

    runApp(const DemoStart());
  }

  @override
  State<DemoStart> createState() => _DemoStartState();
}

class _DemoStartState extends State<DemoStart> {
  late SignCubit _signCubit;
  late AuthCubit _authCubit;
  late PostsCubit _postsCubit;

  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);

    _signCubit = SignCubit();
    _authCubit = AuthCubit();
    _postsCubit = di.sl<PostsCubit>();
  }

  @override
  void dispose() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final appRouter = AppRouter();
    return MultiBlocProvider(
      providers: [
        BlocProvider<SignCubit>.value(value: _signCubit),
        BlocProvider<AuthCubit>.value(value: _authCubit),
        BlocProvider<PostsCubit>.value(value: _postsCubit),
      ],
      child: MaterialApp(
        localizationsDelegates: [
          AppLocalizations.delegate, // Add this line
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: [
          Locale('en'), // English
          Locale('fr'), // Spanish
        ],
        initialRoute: appRouter.initialRoute,
        onGenerateInitialRoutes: appRouter.onGenerateInitialRoutes,
        onGenerateRoute: appRouter.onGenerateRoute,
        theme: ThemeData(
          textTheme: TextThemeApp.regular(),
          colorScheme: ColorSchemeApp.demoLight(),
        ),
      ),
    );
  }
}
