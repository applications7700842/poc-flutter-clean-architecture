import 'package:flutter/material.dart';

class ItemWidget extends StatelessWidget {
  const ItemWidget({
    required this.icon,
    required this.title,
    required this.route,
    super.key,
  });

  final IconData icon;
  final String title;
  final String route;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        ElevatedButton(
          onPressed: () {
            Navigator.pushNamed(context, route);
          },
          child: Icon(icon, size: 50),
          style: ElevatedButton.styleFrom(
            fixedSize: Size(150, 150),
            shape: CircleBorder(),
            padding: EdgeInsets.all(20),
            backgroundColor: Colors.black,
            foregroundColor: Colors.white,
          ),
        ),
        Text(
          title,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ],
    );
  }
}
