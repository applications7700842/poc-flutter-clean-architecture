import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import '../../../core/constants/route_list.dart';
import 'widgets/item_widget.dart';

class MenuPage extends StatelessWidget {
  const MenuPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 30),
        child: GridView.count(
          crossAxisCount: 2,
          mainAxisSpacing: 50,
          children: [
            ItemWidget(
              icon: Icons.language,
              title: AppLocalizations.of(context)!.internationalization,
              route: RouteList.internationalization,
            ),
            ItemWidget(
              icon: Icons.web,
              title: AppLocalizations.of(context)!.httpGetTitle,
              route: '',
            ),
            ItemWidget(
              icon: Icons.phonelink_setup_outlined,
              title: AppLocalizations.of(context)!.localGetPostTitle,
              route: '',
            ),
            ItemWidget(
              icon: Icons.font_download,
              title: AppLocalizations.of(context)!.fontTitle,
              route: '',
            ),
            ItemWidget(
              icon: Icons.color_lens,
              title: AppLocalizations.of(context)!.themeTitle,
              route: '',
            ),
          ],
        ),
      ),
    );
  }
}
