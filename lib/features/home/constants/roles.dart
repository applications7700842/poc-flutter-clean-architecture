enum Role {
  admin('Admin'),
  user('User');

  const Role(this.name);
  final String name;
}
