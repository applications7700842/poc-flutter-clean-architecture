import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../../core/constants/route_list.dart';
import 'cubit/sign_cubit.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SignCubit, SignState>(
      builder: (context, signState) {
        return Scaffold(
          body: Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, top: 300),
            child: Column(
              children: [
                Text(
                  signState.when(
                    signIn: () => AppLocalizations.of(context)!.connection,
                    signUp: () => AppLocalizations.of(context)!.registration,
                  ),
                  style: TextStyle(fontSize: 40),
                ),
                SizedBox(height: 50),
                Form(
                  child: Column(
                    children: [
                      TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        textInputAction: TextInputAction.next,
                        onFieldSubmitted: (value) {},
                        decoration: InputDecoration(
                          hintText: AppLocalizations.of(context)!.emailField,
                          prefixIcon: Icon(Icons.person_outlined),
                        ),
                      ),
                      SizedBox(height: 20),
                      TextFormField(
                        obscureText: true,
                        onFieldSubmitted: (value) {},
                        decoration: InputDecoration(
                          hintText: AppLocalizations.of(context)!.passwordField,
                          prefixIcon: Icon(Icons.lock_outline),
                        ),
                      ),
                      SizedBox(height: 20),
                      signState.maybeWhen(
                        signUp: () => TextFormField(
                          obscureText: true,
                          onFieldSubmitted: (value) {},
                          decoration: InputDecoration(
                            hintText: AppLocalizations.of(context)!
                                .confirmPasswordField,
                            prefixIcon: Icon(Icons.lock_outline),
                          ),
                        ),
                        orElse: SizedBox.new,
                      ),
                      SizedBox(height: 50),
                    ],
                  ),
                ),
                SizedBox(
                  width: double.infinity,
                  child: FilledButton(
                    onPressed: () {
                      Navigator.pushNamed(context, RouteList.menuPage);
                    },
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(Colors.black),
                      shape: MaterialStateProperty.all(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5),
                        ),
                      ),
                      padding: MaterialStateProperty.all(
                        EdgeInsets.symmetric(vertical: 15),
                      ),
                    ),
                    child: Text(
                      signState.when(
                        signIn: () => AppLocalizations.of(context)!.signIn,
                        signUp: () => AppLocalizations.of(context)!.signUp,
                      ),
                      style: TextStyle(color: Colors.white, fontSize: 20),
                    ),
                  ),
                ),
                Row(
                  children: [
                    Text(
                      signState.when(
                        signIn: () =>
                            AppLocalizations.of(context)!.noAccountCreated,
                        signUp: () =>
                            AppLocalizations.of(context)!.alreadyHaveAccount,
                      ),
                    ),
                    TextButton(
                      onPressed: () {
                        context.read<SignCubit>().switchSignMethod();
                      },
                      child: Text(
                        signState.when(
                          signIn: () => AppLocalizations.of(context)!.register,
                          signUp: () =>
                              AppLocalizations.of(context)!.connection,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
