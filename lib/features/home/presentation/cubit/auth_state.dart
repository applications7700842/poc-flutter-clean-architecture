part of 'auth_cubit.dart';

@freezed
class AuthState with _$AuthState {
  const factory AuthState.roleState({required Role role}) = _RoleState;
}
