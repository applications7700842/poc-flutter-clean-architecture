part of 'sign_cubit.dart';

@freezed
class SignState with _$SignState {
  const factory SignState.signIn() = _SignIn;
  const factory SignState.signUp() = _SignUp;
}
