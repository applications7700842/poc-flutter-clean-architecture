import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../constants/roles.dart';

part 'auth_state.dart';
part 'auth_cubit.freezed.dart';

class AuthCubit extends Cubit<AuthState> {
  AuthCubit() : super(AuthState.roleState(role: Role.user));

  void setRoleToAdmin() {
    emit(const AuthState.roleState(role: Role.admin));
  }

  void setRoleToUser() {
    emit(const AuthState.roleState(role: Role.user));
  }
}
