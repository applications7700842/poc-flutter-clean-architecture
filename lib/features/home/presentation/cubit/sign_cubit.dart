import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'sign_state.dart';
part 'sign_cubit.freezed.dart';

class SignCubit extends Cubit<SignState> {
  SignCubit() : super(SignState.signIn());

  void switchSignMethod() {
    state.when(
      signIn: () => emit(SignState.signUp()),
      signUp: () => emit(SignState.signIn()),
    );
  }
}
