part of '../../../di/injection_container.dart';

void _featureSign() {
  sl
    // Data sources

    // Repository

    // Usecases

    //Blocs
    ..injectBloc<SignCubit>(
      SignCubit.new,
    );
}
