import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class InternationalizationPage extends StatefulWidget {
  const InternationalizationPage({super.key});

  @override
  State<InternationalizationPage> createState() =>
      _InternationalizationPageState();
}

class _InternationalizationPageState extends State<InternationalizationPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () {
                Locale('en');
              },
              child: Text('Change language'),
            ),
            Text(
              AppLocalizations.of(context)!.internationalization,
            ),
          ],
        ),
      ),
    );
  }
}
