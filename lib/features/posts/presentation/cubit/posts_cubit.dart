import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../core/errors/failures.dart';
import '../../domain/entities/post.dart';
import '../../domain/usecases/get_posts_usecase.dart';

part 'posts_state.dart';
part 'posts_cubit.freezed.dart';

class PostsCubit extends Cubit<PostsState> {
  PostsCubit({required this.getPostsUsecase}) : super(PostsState.initial());

  final GetPostsUsecase getPostsUsecase;

  Future<void> getPosts() async {
    emit(PostsState.loading());

    final response = await getPostsUsecase.call();

    emit(
      response.when(
        success: (posts) => PostsState.loaded(posts: posts),
        error: (failure) => PostsState.error(failure: failure),
      ),
    );
  }
}
