import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../core/widgets/scaffold_widget.dart';
import '../../home/constants/roles.dart';
import '../../home/presentation/cubit/auth_cubit.dart';
import 'cubit/posts_cubit.dart';
import 'widgets/posts_list_widget.dart';

class PostsPage extends StatefulWidget {
  const PostsPage({super.key});

  @override
  State<PostsPage> createState() => _PostsPageState();
}

class _PostsPageState extends State<PostsPage> {
  @override
  void initState() {
    super.initState();

    context.read<PostsCubit>().getPosts();
    context.read<AuthCubit>();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthCubit, AuthState>(
      builder: (context, stateAuth) {
        return ScaffoldWidget(
          title: 'Page des posts',
          backgroundColor: stateAuth.when(
            roleState: (role) =>
                role == Role.user ? Colors.amber : Colors.black,
          ),
          body: BlocBuilder<PostsCubit, PostsState>(
            builder: (context, state) {
              return state.when(
                initial: () => const SizedBox(),
                loading: () => Center(child: const CircularProgressIndicator()),
                loaded: (posts) {
                  return Center(
                    child: PostsListWidget(posts: posts),
                  );
                },
                error: (failure) => Center(
                  child: Text(failure.message ?? 'Error'),
                ),
              );
            },
          ),
        );
      },
    );
  }
}
