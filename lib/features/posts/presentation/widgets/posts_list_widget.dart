import 'package:flutter/material.dart';

import '../../domain/entities/post.dart';

class PostsListWidget extends StatelessWidget {
  const PostsListWidget({required this.posts, super.key});

  final List<Post> posts;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: posts.length,
      itemBuilder: (context, index) {
        final post = posts[index];
        return ListTile(
          title: Text(post.title),
          subtitle: Text(post.body),
          leading: Text('Post ${post.id}'),
          trailing: Text('Utilisateur ${post.userId}'),
        );
      },
    );
  }
}
