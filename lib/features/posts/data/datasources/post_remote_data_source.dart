import '../../../../core/constants/api_constants.dart';
import '../../../../core/data/datasources/remote_data_source.dart';

abstract class PostRemoteDataSource {
  Future<List<dynamic>> getPosts();
}

class PostRemoteDataSourceImpl extends RemoteDataSource
    implements PostRemoteDataSource {
  PostRemoteDataSourceImpl({required super.dio});

  @override
  Future<List> getPosts() {
    return performGetRequestApi(apiEndpoint: ApiConstants.postsPath);
  }
}
