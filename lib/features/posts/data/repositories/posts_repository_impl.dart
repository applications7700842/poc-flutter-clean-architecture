import '../../../../core/data/repositories/base_repository.dart';
import '../../../../core/domain/result.dart';
import '../../domain/entities/post.dart';
import '../../domain/repositories/posts_repository.dart';
import '../datasources/post_remote_data_source.dart';
import '../models/post_model.dart';

class PostsRepositoryImpl with BaseRepository implements PostsRepository {
  PostsRepositoryImpl({required this.postRemoteDataSource});

  final PostRemoteDataSource postRemoteDataSource;

  @override
  Future<Result<List<Post>>> getPosts() async {
    try {
      final List results = await postRemoteDataSource.getPosts();

      final List<Post> list = List<Post>.from(
        results
            .where((e) => e != null)
            .map((e) => PostModel.fromJson(e).toDomain()),
      );
      return Result.success(list);
    } catch (e) {
      return Result.error(dispatchError(e));
    }
  }
}
