part of '../../../di/injection_container.dart';

void _featurePosts() {
  sl
    // Data sources
    ..injectDataSource<PostRemoteDataSource>(
      () => PostRemoteDataSourceImpl(dio: dio),
    )

    // Repository
    ..injectRepository<PostsRepository>(
      () => PostsRepositoryImpl(postRemoteDataSource: sl()),
    )

    // Usecases
    ..injectUseCase<GetPostsUsecase>(
      () => GetPostsUsecase(repository: sl()),
    )

    //Blocs
    ..injectBloc<PostsCubit>(
      () => PostsCubit(getPostsUsecase: sl()),
    );
}
