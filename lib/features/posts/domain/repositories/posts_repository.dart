import '../../../../core/domain/result.dart';
import '../entities/post.dart';

abstract class PostsRepository {
  Future<Result<List<Post>>> getPosts();
}
