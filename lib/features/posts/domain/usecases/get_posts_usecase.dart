import '../../../../core/domain/result.dart';
import '../entities/post.dart';
import '../repositories/posts_repository.dart';

class GetPostsUsecase {
  GetPostsUsecase({required this.repository});

  final PostsRepository repository;

  Future<Result<List<Post>>> call() async {
    return repository.getPosts();
  }
}
