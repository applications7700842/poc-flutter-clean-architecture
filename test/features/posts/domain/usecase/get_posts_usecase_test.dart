import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:poc_flutter_clean_archi/core/domain/result.dart';
import 'package:poc_flutter_clean_archi/features/posts/domain/entities/post.dart';
import 'package:poc_flutter_clean_archi/features/posts/domain/repositories/posts_repository.dart';
import 'package:poc_flutter_clean_archi/features/posts/domain/usecases/get_posts_usecase.dart';

import 'get_posts_usecase_test.mocks.dart';

@GenerateMocks([PostsRepository])
void main() {
  test('description', () async {
    final PostsRepository repository = MockPostsRepository();
    final GetPostsUsecase usecase = GetPostsUsecase(repository: repository);
    final matcher = Future(
      () => Result.success(
        [Post(userId: 0, id: 0, title: 'title', body: 'body')],
      ),
    );
    when(repository.getPosts()).thenReturn(matcher);

    final response = usecase.call();

    expect(response, matcher);
  });
}
